package info.scce.dime.objectlang.sib.transformationProviders

import info.scce.dime.modeltrafo.extensionpoint.transformation.ISIBtoProcessTransformer
import info.scce.dime.objectlang.objectLang.ObjectsModel
import org.eclipse.core.resources.IFile
import org.eclipse.core.resources.IContainer
import org.eclipse.core.resources.IProject
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.emf.common.util.URI
import org.eclipse.core.resources.ResourcesPlugin
import org.eclipse.core.runtime.IPath
import info.scce.dime.objectlang.sib.transformator.ObjectLangProcessGenerator
import org.eclipse.core.runtime.Path
import static extension info.scce.dime.generator.util.EclipseUtils.*
import org.eclipse.core.resources.IFolder

class ObjectsModelToProcessTransformer implements ISIBtoProcessTransformer<ObjectsModel> {
	
	override transform(ObjectsModel objectsModel) {
		var IFile file = getFile(objectsModel.eResource)
		var IContainer parent = file.parent
		while (!(parent instanceof IProject)) {
			parent = parent.parent
		}
		var IPath outlet = parent.fullPath
		outlet = outlet.removeLastSegments(1).append("gen")
		val IFolder outputFolder = parent.getFolder(outlet)
		outputFolder.mkdirs
		return ObjectLangProcessGenerator.generateProcess(
			objectsModel, outputFolder.fullPath, objectsModel.fileName, objectsModel.generations.head?.modelId
		)
	}
	
	static protected def IFile getFile(Resource resource) {
		if (resource !== null) {
			var URI uri = resource.getURI();
			uri = resource.getResourceSet().getURIConverter().normalize(uri);
			var String scheme = uri.scheme();
			if ("platform".equals(scheme) && uri.segmentCount() > 1 && "resource".equals(uri.segment(0))) {
				var StringBuffer platformResourcePath = new StringBuffer();
				for (var int j = 1, var size = uri.segmentCount(); j < size; j++) {
					platformResourcePath.append('/');
					platformResourcePath.append(uri.segment(j));
				}
				return ResourcesPlugin.getWorkspace().getRoot().getFile(new Path(platformResourcePath.toString()));
			}
		}
		return null;
	}
	
	protected def String getFileName(ObjectsModel model) {
		model.name.lastSegment
	}
	
	protected def String getLastSegment(String fqn) {
		var String[] segments = fqn.split("\\.");
		if (segments.length>0) {			
			return segments.get(segments.length-1);
		}
		return fqn;
	}
}
