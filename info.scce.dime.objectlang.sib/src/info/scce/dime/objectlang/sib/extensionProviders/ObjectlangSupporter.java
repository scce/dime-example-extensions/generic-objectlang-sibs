package info.scce.dime.objectlang.sib.extensionProviders;

import java.net.URL;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.ecore.EClass;
import org.osgi.framework.Bundle;

import info.scce.dime.objectlang.sib.modellingProviders.ObjectlangSIBModellingProvider;
import info.scce.dime.objectlang.sib.transformationProviders.ObjectsModelToProcessTransformer;
import info.scce.dime.modeltrafo.extensionpoint.buildmodel.IGenericSIBBuilder;
import info.scce.dime.modeltrafo.extensionpoint.trafosupport.IModeltrafoSupporter;
import info.scce.dime.modeltrafo.extensionpoint.transformation.ISIBGenerationProvider;
import info.scce.dime.objectlang.objectLang.ObjectLangPackage;
import info.scce.dime.objectlang.objectLang.ObjectsModel;

public class ObjectlangSupporter implements IModeltrafoSupporter<ObjectsModel> {
	
	private static ObjectlangSIBModellingProvider modellingProvider = new ObjectlangSIBModellingProvider();
	private static ObjectsModelToProcessTransformer generationProvider = new ObjectsModelToProcessTransformer();

	@Override
	public String getModelExtension() {
		return "objects";
	}

	@Override
	public String getSIBName() {
		return "ObjectLanguageSIB";
	}

	@Override
	public String getIconPath() {
		Bundle bundle = Platform.getBundle("info.scce.dime.objectlang.sib");
		URL url = FileLocator.find(bundle, new Path("icons/objectlang.png"), null);
		try {
			URL fileURL = FileLocator.toFileURL(url);
			return fileURL.getFile();
		} catch (Exception e) {
			throw new IllegalStateException(e);
		}
	}

	@Override
	public IGenericSIBBuilder<ObjectsModel> getModellingProvider() {
		return modellingProvider;
	}

	@Override
	public EClass getModelType() {
		return ObjectLangPackage.eINSTANCE.getObjectsModel();
	}

	@Override
	public ISIBGenerationProvider getGenerationProvider() {
		return generationProvider;
	}

}
