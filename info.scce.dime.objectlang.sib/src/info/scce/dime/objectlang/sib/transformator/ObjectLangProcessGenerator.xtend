package info.scce.dime.objectlang.sib.transformator

import de.jabc.cinco.meta.core.ge.style.generator.runtime.layout.EdgeLayout
import de.jabc.cinco.meta.core.referenceregistry.ReferenceRegistry
import de.jabc.cinco.meta.runtime.xapi.GraphModelExtension
import graphmodel.Container
import graphmodel.Node
import graphmodel.internal.InternalModelElement
import info.scce.dime.api.modelgen.ProcessModelGenerationLanguage
import info.scce.dime.data.data.Attribute
import info.scce.dime.data.data.ComplexAttribute
import info.scce.dime.data.data.EnumLiteral
import info.scce.dime.data.data.PrimitiveAttribute
import info.scce.dime.data.data.PrimitiveType
import info.scce.dime.data.data.Type
import info.scce.dime.data.data.UserType
import info.scce.dime.data.helper.DataExtension
import info.scce.dime.objectlang.ObjectLangExtension
import info.scce.dime.objectlang.generator.ObjectsModelProcessSIBBuild
import info.scce.dime.objectlang.objectLang.AttributeDef
import info.scce.dime.objectlang.objectLang.AttributeValueDef
import info.scce.dime.objectlang.objectLang.AttributeValueReferenceDef
import info.scce.dime.objectlang.objectLang.BooleanAttributeValueDef
import info.scce.dime.objectlang.objectLang.BooleanListAttributeValueDef
import info.scce.dime.objectlang.objectLang.ComplexListAttributeValuesDef
import info.scce.dime.objectlang.objectLang.InputDef
import info.scce.dime.objectlang.objectLang.IntegerAttributeValueDef
import info.scce.dime.objectlang.objectLang.IntegerListAttributeValueDef
import info.scce.dime.objectlang.objectLang.NamedObject
import info.scce.dime.objectlang.objectLang.ObjectCreation
import info.scce.dime.objectlang.objectLang.ObjectDef
import info.scce.dime.objectlang.objectLang.ObjectInstantiation
import info.scce.dime.objectlang.objectLang.ObjectsModel
import info.scce.dime.objectlang.objectLang.PrimitiveAttributeValueDef
import info.scce.dime.objectlang.objectLang.PrimitiveListAttributeValueDef
import info.scce.dime.objectlang.objectLang.ProcessOutput
import info.scce.dime.objectlang.objectLang.RealAttributeValueDef
import info.scce.dime.objectlang.objectLang.RealListAttributeValueDef
import info.scce.dime.objectlang.objectLang.TextAttributeValueDef
import info.scce.dime.objectlang.objectLang.TextListAttributeValueDef
import info.scce.dime.process.factory.ProcessFactory
import info.scce.dime.process.helper.PortUtils
import info.scce.dime.process.process.BooleanInputStatic
import info.scce.dime.process.process.ComplexInputPort
import info.scce.dime.process.process.ComplexOutputPort
import info.scce.dime.process.process.DataContext
import info.scce.dime.process.process.DataFlow
import info.scce.dime.process.process.DataFlowSource
import info.scce.dime.process.process.DataFlowTarget
import info.scce.dime.process.process.EndSIB
import info.scce.dime.process.process.InputPort
import info.scce.dime.process.process.IntegerInputStatic
import info.scce.dime.process.process.Output
import info.scce.dime.process.process.OutputPort
import info.scce.dime.process.process.PrimitiveInputPort
import info.scce.dime.process.process.PrimitiveOutputPort
import info.scce.dime.process.process.Process
import info.scce.dime.process.process.ProcessSIB
import info.scce.dime.process.process.Read
import info.scce.dime.process.process.RealInputStatic
import info.scce.dime.process.process.SIB
import info.scce.dime.process.process.StartSIB
import info.scce.dime.process.process.TextInputStatic
import info.scce.dime.process.process.TimestampInputStatic
import java.util.Collection
import java.util.Collections
import java.util.List
import org.eclipse.core.runtime.IPath
import org.eclipse.emf.ecore.EObject

import static info.scce.dime.process.factory.ProcessFactory.*

import static extension info.scce.dime.process.helper.SIBLayoutUtils.resizeAndLayout
import static extension org.eclipse.emf.ecore.util.EcoreUtil.setID

/**
 * Generates an object creation process from an ObjectsModel.
 * 
 * @author Steve Bosselmann
 */
class ObjectLangProcessGenerator {
	
	extension GraphModelExtension = new GraphModelExtension
	extension DataExtension = DataExtension.instance
	extension ObjectLangExtension = new ObjectLangExtension
	extension ProcessModelGenerationLanguage = new ProcessModelGenerationLanguage
	
	val ObjectsModel model
	var Process process
	val previousBranches = <DataFlowSource> newArrayList
	val lastInCol = <ObjectCreation,DataFlowSource> newHashMap
	val objCrt_on_port = <ObjectCreation,ComplexOutputPort> newLinkedHashMap
	val inputDef_on_port = <InputDef,OutputPort> newLinkedHashMap
	
	static def generateProcess(ObjectsModel model, IPath outlet, String fileName, String modelId) {
		new ObjectLangProcessGenerator(model).generateProcess(outlet, fileName, modelId)
	}
	
	new(ObjectsModel model) {
		this.model = model
	}
	
	def Process generateProcess(IPath outlet, String fileName, String modelId) {
		debug("Generating process")
		debug("  outlet: " + outlet)
		debug("  fileName: " + fileName)
		debug("  modelId: " + modelId)
		/*
		 * workaround that forces the use of the basic ProcessFactory
		 */
		val orgProcessFactory = ProcessFactory.eINSTANCE
		try {
			ProcessFactory.eINSTANCE = new ProcessFactory
			// make ProcessModelGenerationLanguage use the new ProcessFactory.eINSTANCE
			_processModelGenerationLanguage = new ProcessModelGenerationLanguage
			process = createBasicProcess(outlet, fileName, false, false)
			process.transact("Generate process model " + fileName) [
				if (!modelId.nullOrEmpty) {
					process.setID(modelId)
				}
				process.modelName = fileName
				generateProcessComponents
				process.save
				debug(" process saved.")
				info(fileName + " (" + process?.id + ") created at path " + outlet)
			]
		} catch(Exception e) {
			throw new RuntimeException(e)
		} finally {
			ProcessFactory.eINSTANCE = orgProcessFactory
		}
		return process;
	}
	
	def generateProcessComponents() {
		previousBranch = process.newStartSIB(50,50, 100, 65) => [
			generateProcessInputPorts
		]
		
		for (objDef : model.objects.filter[!isAbstract && !hasBeenCreated]) {
			generateObjectCreation(objDef)
		}
		
		val endSIB = process.newEndSIB(0,0,100,65) => [
			generateProcessOutputPorts
		]
		for (previousBranch : previousBranches) {
			previousBranch.successor = endSIB
		}
		
		debug(" apply manhattan layout to all edges")
		process.allEdges.filter(DataFlow).forEach[ EdgeLayout.C_LEFT.apply(it) ]
		
		debug(" process components generated.")
	}
	
	def generateProcessInputPorts(StartSIB startSIB) {
		for (input : model.inputs) {
			debug("  creating StartSIB port for input " + input?.name + " (" + (input.primitiveType ?: input.complexType) + ")")
			if (input.hasPrimitiveType) {
				startSIB.addOutputPort(input.primitiveType, input.name) => [
					inputDef_on_port.put(input, it)
				]
			} else if (input.hasComplexType) {
				startSIB.addOutputPort(input.complexType) => [
					it.name = input.name
					inputDef_on_port.put(input, it)
				]
			}
		}
	}
	
	def generateProcessOutputPorts(EndSIB endSIB) {
		val outObjs = model.objects.filter[it.output !== null]
		for (outObj : outObjs) {
			endSIB.addInputPort(outObj.type) => [
				it.name = outObj.output.alias ?: outObj.name
				dataFlowFrom(getObjectProvidingPort(outObj))
			]
		}
	}
	
	dispatch def SIB generateObjectCreation(ObjectDef objDef) {
		val sib = generateObjectCreation(objDef, objDef.creation)
		
		if (!objDef.isReferenced) {
			lastInCol.remove(objDef)
		}
		return sib
	}
	
	dispatch def SIB generateObjectCreation(ObjectCreation objCrt) {
		generateObjectCreation(objCrt, objCrt.hierarchicalAttributes)
	}
	
	dispatch def SIB generateObjectCreation(ObjectInstantiation objInst, Iterable<AttributeDef> attributeDefs) {
		val sib = generateCreateSIB(objInst)
		
		val successBranch = sib.getBranch("success")
		val createdPort = successBranch.findThe(ComplexOutputPort)[name == "created"] => [
			resize(90,18)
		]
		objCrt_on_port.put(objInst, createdPort)

		generatePrimitiveSetters(sib, objInst, attributeDefs)
		generatePrimitiveListSetters(objInst, attributeDefs)
		generateComplexSetters(objInst, attributeDefs)
		generateComplexListSetters(objInst, attributeDefs)
		
		successBranch.alignBeneathOf(sib)
		
		return sib
	}
	
	dispatch def SIB generateObjectCreation(ProcessOutput proOut, Iterable<AttributeDef> attributeDefs) {
		val sib = generateProcessSIB(proOut)
		
		generatePrimitiveSetters(proOut, attributeDefs)
		generatePrimitiveListSetters(proOut, attributeDefs)
		generateComplexSetters(proOut, attributeDefs)
		generateComplexListSetters(proOut, attributeDefs)
		
		return sib
	}
	
	dispatch def SIB generateObjectCreation(ObjectDef objDef, ObjectInstantiation objInst) {
		val sib = generateObjectCreation(objInst)
		
		val type = objDef.typeDef?.type as Type
		val internal = sib.findThe(PrimitiveInputPort)[name == "internalName"]
		internal.putStaticValue(
			type.name + if (!objDef.getName.nullOrEmpty) ''' «objDef.getName»''' else '')
		
		generateDocumentation(objDef, sib)
		
		return sib
	}
	
	dispatch def SIB generateObjectCreation(ObjectDef objDef, ProcessOutput proOut) {
		val sib = generateObjectCreation(proOut)
		
		generateDocumentation(objDef, sib)
		
		return sib
	}
	
	/**
	 * Generates
	 *   - CreateSIB for a ConcreteType
	 *   - CreateUserSIB for a UserType
	 */
	def generateCreateSIB(ObjectInstantiation objInst) {
		debug("  create CreateSIB for objInst: " + objInst)
		val type = objInst.typeDef?.type as Type
		val sib =
			if (type instanceof UserType)
				process.newCreateUserSIB(type, 0,0)
			else process.newCreateSIB(type, 0,0)
		debug("  => " + sib)
		
		sib.addToControlFlow(objInst)
		val successBranch = sib.getBranch("success")
		successBranch.addToControlFlow(objInst)
		
		return sib
	}
	
	def generateProcessSIB(ProcessOutput processOutput) {
		val refProcess = getImportedProcessModel(processOutput.processDef)
		if (refProcess !== null) {
			generateProcessSIB(refProcess, processOutput)
		} else {
			val objectsModel = getImportedObjectsModel(processOutput.processDef)
			generateProcessSIB(objectsModel, processOutput)
		}
	}
	
	def generateProcessSIB(ObjectsModel objectsModel, ProcessOutput processOutput) {
		val refID = objectsModel.generations.map[it.modelId].head
		debug("  create ProcessSIB for objects model: " + objectsModel)
		val processSIB = ProcessFactory.eINSTANCE.createProcessSIB => [
			it.libraryComponentUID = refID
			it.label = objectsModel.targetFileName
			it.width = 120
		]
		process.internalContainerElement.modelElements.add(processSIB.internalElement as InternalModelElement)
		debug("  => " + processSIB)
		
		ObjectsModelProcessSIBBuild.initialize(processSIB, objectsModel)
		
		generateProcessSIBInputs(processSIB, processOutput)
		
		val output = objectsModel.objects.findFirst[it.output !== null]
		if (output !== null) {
			val outputBranch = processSIB.getBranch("success")
			outputBranch.addToControlFlow(processOutput)
			val createdPort = outputBranch.findThe(OutputPort)[it.name == output.name]
			if (createdPort instanceof ComplexOutputPort) {
				objCrt_on_port.put(processOutput, createdPort)
			}
		}
		return processSIB
	}
	
	def generateProcessSIB(Process refProcess, ProcessOutput processOutput) {
		debug("  create ProcessSIB for process: " + refProcess?.modelName)
		val processSIB = process.newProcessSIB(refProcess, 0,0)
		debug("  => " + processSIB)
		
		generateProcessSIBInputs(processSIB, processOutput)
		
		val output = 
			processOutput.outputDef?.output
			?: processSIB.proMod?.find(EndSIB)?.flatMap[inputPorts]?.head
		if (output instanceof InputPort) {
			val endSib = output.container as EndSIB
			val outputBranch = processSIB.getBranch(endSib.branchName)
			outputBranch.addToControlFlow(processOutput)
			var lastBranch = outputBranch
			for (otherBranch : processSIB.branchSuccessors.filter[it != outputBranch]) {
				otherBranch.move(lastBranch.right + 10, lastBranch.bottom + 10)
				previousBranches.add(otherBranch)
				lastBranch = otherBranch
			}
			val createdPort = outputBranch.findThe(OutputPort)[it.name == output.name]
			if (createdPort instanceof ComplexOutputPort) {
				objCrt_on_port.put(processOutput, createdPort)
			}
		}
		return processSIB
	}
	
	def generateProcessSIBInputs(ProcessSIB processSIB, ProcessOutput processOutput) {
		// generate static inputs
		for (arg : processOutput.arguments) {
			val inputDef = arg.key
			val inputName = switch inputDef {
				OutputPort: inputDef.name
				InputDef: inputDef.name
			}
			val hasEnumType = switch inputDef {
				OutputPort: inputDef.isEnumPort
				InputDef: false
			}
			val hasFileType = switch inputDef {
				OutputPort: inputDef.hasPrimitiveType(PrimitiveType.FILE)
				InputDef: inputDef.hasPrimitiveType(PrimitiveType.FILE)
			}
			val primitiveType = switch inputDef {
				OutputPort: inputDef.primitiveType
				InputDef: inputDef.primitiveType
			}
			
			val sibInputPort = processSIB.findThe(InputPort)[it.name == inputName]
			switch valueDef : arg.valueDef {
				
				TextAttributeValueDef case valueDef.isHashed: {
					val valOutput = generateHashedTextProvider(processOutput, valueDef.value)
					valOutput.dataFlowTo(sibInputPort)
				}
					
				TextAttributeValueDef case sibInputPort.hasPrimitiveType(PrimitiveType.FILE): {
					val valOutput = generateFileProvider(processOutput, valueDef.value)
					valOutput.dataFlowTo(sibInputPort)
				}
					
				TextAttributeValueDef case sibInputPort.isEnumPort: {
					val enumType = arg.complexType
					val literal = enumType.enumLiterals.findFirst[it.name == valueDef.value]
					val valOutput = generateEnumLiteralProvider(processOutput, literal)
					valOutput.dataFlowTo(sibInputPort)
				}
					
				IntegerAttributeValueDef case sibInputPort.hasPrimitiveType(PrimitiveType.TIMESTAMP):
					sibInputPort.putStaticValue(valueDef)
				
				// fall-through case for other primitives
				PrimitiveAttributeValueDef:
					sibInputPort.putStaticValue(valueDef)
				
				PrimitiveListAttributeValueDef: {
					val values = valueDef.values
					val valOutputs = newArrayList
	
					if (!values.nullOrEmpty) {
					
						val putToContextSIB = process.newPutToContextSIB(0,0) => [ resize(120,65) ]
						var successBranch = putToContextSIB.getBranch("success") => [ resize(120,25) ]
						var index = 0
						for (value : values) {
							val name = "val" + index++
							debug("   startSIBPort: " + inputName + " isEnum: " + hasEnumType)
							if (hasEnumType) {
								debug(" generate enum literal setter for argDef: " + arg)
								val enumType = (inputDef as OutputPort).complexType
								val literal = enumType.enumLiterals.findFirst[it.name == value?.toString]
								val valOutput = generateEnumLiteralProvider(processOutput, literal)
								valOutputs.add(valOutput)
							} else {
								if (hasFileType) {
									debug(" generate file setter for argDef: " + arg)
									val valOutput = generateFileProvider(processOutput, value?.toString)
									valOutputs.add(valOutput)
								} else if (valueDef.isHashed) {
									debug(" generate hashed password setter for argDef: " + arg)
									val valOutput = generateHashedTextProvider(processOutput, value?.toString)
									valOutputs.add(valOutput)
								} else {
									putToContextSIB.addStaticInputPort(primitiveType, name, value) => [ resize(90,18) ]
									successBranch.addOutputPort(primitiveType, name) => [
										resize(90,18)
										valOutputs.add(it)
									]
								}
							}
						}
						
						putToContextSIB.addToControlFlow(processOutput)
						successBranch.addToControlFlow(processOutput)
						
						val variable = generateInputVariable(sibInputPort)
						for (valOutput : valOutputs) {
							valOutput.dataFlowTo(variable) => [ EdgeLayout.C_LEFT.apply(it) ]
						}
					}
				}
				
				ObjectCreation, AttributeValueReferenceDef: {
					generateRead(sibInputPort, valueDef)
				}
				
				ComplexListAttributeValuesDef: {
					generateComplexListRead(sibInputPort as ComplexInputPort, valueDef, processOutput)
				}
			}
		}
		
		processSIB.addToControlFlow(processOutput)
	}
	
	def generateDocumentation(ObjectDef objDef, Node node) {
		debug("  create Documentation for objDef: " + objDef)
		val content = objDef.getName ?: "<unnamed>"
		val width = Math.max(node.width, content.length * 5)
		process.newDocumentation(node.x, node.y - 60, width, 50) => [
			it.content = content
		]
	}
	
	def toStatic(PrimitiveInputPort inputPort) {
		val sib = inputPort.container as DataFlowTarget
		val name = inputPort.name
		val type = inputPort.dataType.mapOnDataType
		inputPort.delete
		switch type {
			case BOOLEAN: sib.addStaticBooleanInputPort(name) => [ resize(90,18) ]
			case FILE: throw new RuntimeException("Static File input ports are not supported")
			case INTEGER: sib.addStaticIntegerInputPort(name) => [ resize(90,18) ]
			case REAL: sib.addStaticRealInputPort(name) => [ resize(90,18) ]
			case TIMESTAMP: sib.addStaticTimestampInputPort(name) => [ resize(90,18) ]
			case TEXT: sib.addStaticTextInputPort(name) => [ resize(90,18) ]
		}
	}
	
	def putStaticValue(InputPort inputPort, PrimitiveAttributeValueDef valueDef) {
		switch valueDef {
			BooleanAttributeValueDef: inputPort.putStaticValue(valueDef.value)
			RealAttributeValueDef: inputPort.putStaticValue(valueDef.value)
			IntegerAttributeValueDef: inputPort.putStaticValue(valueDef.value)
			TextAttributeValueDef: inputPort.putStaticValue(valueDef.value)
		}
	}
	
	def putStaticValue(InputPort inputPort, Boolean value) {
		val sib = inputPort.container as DataFlowTarget
		val name = inputPort.name
		inputPort.delete
		sib.addStaticBooleanInputPort(name) => [
			it.value = value
			resize(90,18)
		]
	}
	
	def putStaticValue(InputPort inputPort, Long value) {
		val sib = inputPort.container as DataFlowTarget
		val name = inputPort.name
		inputPort.delete
		sib.addStaticIntegerInputPort(name) => [
			it.value = value
			resize(90,18)
		]
	}
	
	def putStaticValue(InputPort inputPort, Double value) {
		val sib = inputPort.container as DataFlowTarget
		val name = inputPort.name
		inputPort.delete
		sib.addStaticRealInputPort(name) => [
			it.value = value
			resize(90,18)
		]
	}
	
	def putStaticValue(InputPort inputPort, String value) {
		val sib = inputPort.container as DataFlowTarget
		val name = inputPort.name
		inputPort.delete
		sib.addStaticTextInputPort(name) => [
			it.value = value
			resize(90,18)
		]
	}
	
	/**
	 * Generates setters for each primitive attribute of the object created
	 * by the ObjectInstantiation (i.e. ObjectNew or ObjectExtend)
	 */
	def generatePrimitiveSetters(SIB createSIB, ObjectCreation objCrt, Iterable<AttributeDef> attributeDefs) {
		debug(" generate primitive setters for objCrt: " + objCrt)
		attributeDefs.forEach[ attrDef | 
			
			val attribute = attrDef.key as Attribute
			/*
			 * only setters that require an additional SIB for
			 * retrieving the actual value are created here
			 */
			switch valueDef : attrDef.valueDef {
				
				// file attribute
				TextAttributeValueDef case attribute.isFileAttribute:
					generateFileSetter(objCrt, attrDef, valueDef)
				
				// enum attribute
				TextAttributeValueDef case attribute.isEnumAttribute:
					generateEnumLiteralSetter(objCrt, attrDef, valueDef.value)
				
				// hashed password attribute
				TextAttributeValueDef case valueDef.isHashed:
					generateHashedTextSetter(objCrt, attrDef, valueDef.value)
				
				TextAttributeValueDef
					case attribute.isTextAttribute && !valueDef.isHashed:
					createSIB.addStaticTextInputPort(attribute.name) => [
						value = valueDef.value
						resize(90,18)
					]
					
				BooleanAttributeValueDef:
					createSIB.addStaticBooleanInputPort(attribute.name) => [
						value = valueDef.value
						resize(90,18)
					]
				
				IntegerAttributeValueDef case attribute.isIntegerAttribute:
					createSIB.addStaticIntegerInputPort(attribute.name) => [
						value = valueDef.value
						resize(90,18)
					]
				
				IntegerAttributeValueDef case attribute.isTimestampAttribute:
					createSIB.addStaticTimestampInputPort(attribute.name) => [
						value = valueDef.value
						resize(90,18)
					]
				
				RealAttributeValueDef:
					createSIB.addStaticRealInputPort(attribute.name) => [
						value = valueDef.value
						resize(90,18)
					]
				
				AttributeValueReferenceDef: switch it : valueDef.value {
					InputDef case hasPrimitiveType: 
						generateInputValueSetter(objCrt, attrDef, valueDef)
				}
				
				// primitive attribute value from process output
				ProcessOutput case !(attribute instanceof ComplexAttribute):
					generateProcessOutputSetter(objCrt, attrDef, valueDef)
			}
		]
	}
	
	/**
	 * Generates setters for each primitive attribute of the
	 * object created by the ProcessOutput.
	 */
	def generatePrimitiveSetters(ObjectCreation objCrt, Iterable<AttributeDef> attributeDefs) {
		debug(" generate primitive setters for objCrt: " + objCrt)
		attributeDefs.forEach[ attrDef |
			
			val attribute = attrDef.key as Attribute
			switch valueDef : attrDef.valueDef {
				
				PrimitiveAttributeValueDef:
					generatePrimitiveSetter(objCrt, attrDef, valueDef)
					
				AttributeValueReferenceDef: switch it : valueDef.value {
					InputDef case hasPrimitiveType: 
						generateInputValueSetter(objCrt, attrDef, valueDef)
				}
				
				// attribute value from process output
				ProcessOutput case !(attribute instanceof ComplexAttribute):
					generateProcessOutputSetter(objCrt, attrDef, valueDef)
			}
		]
	}
	
	dispatch def generatePrimitiveSetter(ObjectCreation objCrt, AttributeDef attrDef, TextAttributeValueDef valueDef) {
		val attribute = attrDef.key as Attribute
		debug("   attribute: " + attribute?.name + " isEnum: " + attribute.isEnumAttribute)
		if (attribute.isFileAttribute) {
			debug(" generate file setter for attrDef: " + attrDef)
			generateFileSetter(objCrt, attrDef, valueDef)
		} else if (attribute.isEnumAttribute) {
			debug(" generate enum literal setter for attrDef: " + attrDef)
			generateEnumLiteralSetter(objCrt, attrDef, valueDef.value)
		} else if (valueDef.isHashed) {
			debug(" generate hashed password setter for attrDef: " + attrDef)
			generateHashedTextSetter(objCrt, attrDef, valueDef.value)
		} else {
			debug(" generate Text setter for attrDef: " + attrDef)
			val sib = generateStaticSetter(objCrt, attrDef)
			sib.findThe(TextInputStatic) => [
				debug(" TextInputStatic: " + it)
				debug("   -> set value: " + valueDef.value)
				it.value = valueDef.value
				resize(90,18)
			]
		}
	}
	
	dispatch def generatePrimitiveSetter(ObjectCreation objCrt, AttributeDef attrDef, IntegerAttributeValueDef valueDef) {
		val sib = generateStaticSetter(objCrt, attrDef)
		val attr = attrDef.key as Attribute
		switch attr {
			case attr.isTimestampAttribute: {
				debug(" generate Timestamp setter for attrdef: " + attrDef)
				sib.findThe(TimestampInputStatic) => [
					it.value = valueDef.value
					resize(90,18)
				]
			}
			default: {
				debug(" generate Integer setter for attrdef: " + attrDef)
				sib.findThe(IntegerInputStatic) => [
					it.value = valueDef.value
					resize(90,18)
				]
			}
		}
		return sib
	}
	
	dispatch def generatePrimitiveSetter(ObjectCreation objCrt, AttributeDef attrDef, RealAttributeValueDef valueDef) {
		debug(" generate Real setter for attrdef: " + attrDef)
		val sib = generateStaticSetter(objCrt, attrDef)
		sib.findThe(RealInputStatic) => [
			it.value = valueDef.value
			resize(90,18)
		]
		return sib
	}
	
	dispatch def generatePrimitiveSetter(ObjectCreation objCrt, AttributeDef attrDef, BooleanAttributeValueDef valueDef) {
		debug(" generate Boolean setter for attrdef: " + attrDef)
		val sib = generateStaticSetter(objCrt, attrDef)
		sib.findThe(BooleanInputStatic) => [
			it.value = valueDef.value
			resize(90,18)
		]
		return sib
	}
	
	def generateInputValueSetter(ObjectCreation objCrt, AttributeDef attrDef, AttributeValueReferenceDef valueDef) {
		debug(" generate Input value setter for attrdef: " + attrDef)
		val valOutput = getObjectProvidingPort(valueDef.value)
		generateSetAttributeValueSIB(objCrt, attrDef.key as Attribute, valOutput)
	}
	
	def generateFileProvider(ObjectCreation objCrt, String filePath) {
		val nativeDef = ReferenceRegistry.instance.getEObject("common.UploadProjectResource")
		debug(" native 'common.UploadProjectResource' from registry: " + nativeDef)
		val nativeSib = process.newAtomicSIB(nativeDef, 0,0) => [ resize(120,65) ]
		val valInput = nativeSib.findThe(PrimitiveInputPort)
		valInput.putStaticValue(filePath)
		
		nativeSib.getBranch("failure")?.delete
		val successBranch = nativeSib.getBranch("success") => [ resize(120,25) ]
		val valOutput = successBranch.findThe(PrimitiveOutputPort)
		
		nativeSib.addToControlFlow(objCrt)
		successBranch.addToControlFlow(objCrt)
		
		return valOutput
	}
	
	/**
	 * Generates
	 *   - NativeSIB UploadProjectResource to upload a file
	 *   - SetAttributeValueSIB for the file attribute
	 */
	def generateFileSetter(ObjectCreation objCrt, AttributeDef attrDef, TextAttributeValueDef valueDef) {
		val valOutput = generateFileProvider(objCrt, valueDef.value)
		generateSetAttributeValueSIB(objCrt, attrDef.key as Attribute, valOutput)
	}
	
	def generateEnumLiteralProvider(ObjectCreation objCrt, EnumLiteral literal) {
		val retrieveSib = process.newRetrieveEnumLiteralSIB(literal, 0,0) => [ resize(120,65) ]
		val successBranch = retrieveSib.getBranch(literal.name) => [ resize(120,25) ]
		val valOutput = successBranch.findThe(ComplexOutputPort)
		
		retrieveSib.addToControlFlow(objCrt)
		successBranch.addToControlFlow(objCrt)
		
		return valOutput
	}
	
	/**
	 * Generates
	 *   - RetrieveEnumLiteralSIB for the respective literal
	 *   - SetAttributeValueSIB for the enum attribute
	 */
	def generateEnumLiteralSetter(ObjectCreation objCrt, AttributeDef attrDef, String literalName) {
		val literal = attrDef.complexType.enumLiterals.findFirst[it.name == literalName]
		val valOutput = generateEnumLiteralProvider(objCrt, literal)
		generateSetAttributeValueSIB(objCrt, attrDef.key as Attribute, valOutput)
	}
	
	def generateHashedTextProvider(ObjectCreation objCrt, String value) {
		val nativeDef = ReferenceRegistry.instance.getEObject("common.HashPassword")
		debug(" native 'common.HashPassword' from registry: " + nativeDef)
		val nativeSib = process.newAtomicSIB(nativeDef, 0,0) => [ resize(120,65) ]
		val valInput = nativeSib.findThe(PrimitiveInputPort)
		valInput.putStaticValue(value)
		
		val successBranch = nativeSib.getBranch("success") => [ resize(120,25) ]
		val valOutput = successBranch.findThe(PrimitiveOutputPort)
		
		nativeSib.addToControlFlow(objCrt)
		successBranch.addToControlFlow(objCrt)
		
		return valOutput
	}
	
	/**
	 * Generates
	 *   - NativeSIB HashPassword to hash the text
	 *   - SetAttributeValueSIB for the text attribute
	 */
	def generateHashedTextSetter(ObjectCreation objCrt, AttributeDef attrDef, String value) {
		val valOutput = generateHashedTextProvider(objCrt, value)
		generateSetAttributeValueSIB(objCrt, attrDef.key as Attribute, valOutput)
	}
	
	/**
	 * Generates
	 *   - NativeSIB HashPassword to hash the text
	 *   - SetAttributeValueSIB for the text attribute
	 */
	def generateProcessOutputSetter(ObjectCreation objCrt, AttributeDef attrDef, ProcessOutput proOut) {
		val processSIB = generateProcessSIB(proOut)
		
		val output = 
			proOut.outputDef?.output
			?: processSIB.proMod.find(EndSIB).flatMap[inputPorts].filter(InputPort).head
		
		if (output instanceof InputPort) {
			val endSib = output.container as EndSIB
			val outputBranch = processSIB.getBranch(endSib.branchName)
			outputBranch.addToControlFlow(proOut)
			var lastBranch = outputBranch
			for (otherBranch : processSIB.branchSuccessors.filter[it != outputBranch]) {
				otherBranch.move(lastBranch.right + 10, lastBranch.bottom + 10)
				previousBranches.add(otherBranch)
				lastBranch = otherBranch
			}
			val createdPort = outputBranch.findThe(OutputPort)[it.name == output.name]
			if (createdPort instanceof ComplexOutputPort) {
				objCrt_on_port.put(proOut, createdPort)
			}
			generateSetAttributeValueSIB(objCrt, attrDef.key as Attribute, createdPort)
		}
	}
	
	
	def generateStaticSetter(ObjectCreation objCrt, AttributeDef attrDef) {
		debug(" generate primitive static setter for attrdef: " + attrDef)
		val attribute = attrDef.key as PrimitiveAttribute
		val sib = generateSetAttributeValueSIB(objCrt, attribute)
		debug(" SetAttributeValueSIB: " + sib)
		val valInput = sib.find(PrimitiveInputPort).last => [ resize(90,18) ]
		debug(" val InputPort: " + valInput)
		valInput.toStatic
		return sib
	}
	
	/**
	 * Generates a SetAttributeValueSIB for the specified attribute
	 */
	def generateSetAttributeValueSIB(ObjectCreation objCrt, Attribute attr) {
		val sib = process.newSetAttributeValueSIB(attr, 0,0)
		
		val inputPorts = sib.find(InputPort)
		val objInput = inputPorts.head => [ resize(90,18) ]
		val objCreatedPort = getObjectProvidingPort(objCrt)
		objCreatedPort.dataFlowTo(objInput)
		
		inputPorts.last => [ resize(90,18) ]
		
		sib.addToControlFlow(objCrt)
		val successBranch = sib.getBranch("success") => [ resize(120,25) ]
		successBranch.addToControlFlow(objCrt)
		
		return sib
	}
	
	/**
	 * Generates a SetAttributeValueSIB for the specified attribute and connects
	 * the specified Output via DataFlow edge
	 */
	def generateSetAttributeValueSIB(ObjectCreation objCrt, Attribute attr, Output valOutput) {
		val sib = generateSetAttributeValueSIB(objCrt, attr)
		val valInput = sib.find(InputPort).last => [ resize(90,18) ]
		valOutput.dataFlowTo(valInput)
		return sib
	}
	
	/**
	 * Generates setters for primitive list attributes
	 */
	def generatePrimitiveListSetters(ObjectCreation objCrt, Iterable<AttributeDef> attributeDefs) {
		debug(" generate primitive list setters for objCrt: " + objCrt)
		attributeDefs
			.forEach[ attrDef | switch valueDef : attrDef.valueDef {
				PrimitiveListAttributeValueDef:
					generatePrimitiveListSetter(objCrt, attrDef, valueDef)
			}]
	}
	
	/**
	 * Generates
	 *   - PutToContextSIB with InputPorts for each list value
	 *   - SetAttributeValueSIB for the list attribute
	 *   - DataContext with list variable to be filled by the OutputPorts of the PutToContextSIB
	 */
	def generatePrimitiveListSetter(ObjectCreation objCrt, AttributeDef attrDef, PrimitiveListAttributeValueDef valueDef) {
		debug(" generate primitive list setter for attrdef: "
			+ if (attrDef?.key instanceof Attribute) (attrDef.key as Attribute).name else attrDef
		)
		val values = switch it : valueDef {
			BooleanListAttributeValueDef: values
			IntegerListAttributeValueDef: values
			RealListAttributeValueDef: values
			TextListAttributeValueDef: values
			default: #[]
		}
		val valOutputs = newArrayList
		
		if (!values.nullOrEmpty) {
		
			val putToContextSIB = process.newPutToContextSIB(0,0) => [ resize(120,65) ]
			var successBranch = putToContextSIB.getBranch("success") => [ resize(120,25) ]
			var index = 0
			for (value : values) {
				val name = "val" + index++
				val attribute = attrDef.key as Attribute
				
				debug("   attribute: " + attribute?.name + " isEnum: " + attribute.isEnumAttribute)
				if (attribute.isEnumAttribute) {
					debug(" generate enum literal setter for attrDef: " + attrDef)
					val enumType = attrDef.complexType
					val literal = enumType.enumLiterals.findFirst[it.name == value?.toString]
					val valOutput = generateEnumLiteralProvider(objCrt, literal)
					valOutputs.add(valOutput)
				} else {
					if (attribute.isFileAttribute) {
						debug(" generate file setter for attrDef: " + attrDef)
						val valOutput = generateFileProvider(objCrt, value?.toString)
						valOutputs.add(valOutput)
					} else if (valueDef.isHashed) {
						debug(" generate hashed password setter for attrDef: " + attrDef)
						val valOutput = generateHashedTextProvider(objCrt, value?.toString)
						valOutputs.add(valOutput)
					} else {
						val dataType = attribute.primitiveType
						putToContextSIB.addStaticInputPort(dataType, name, value) => [ resize(90,18) ]
						successBranch.addOutputPort(dataType, name) => [
							resize(90,18)
							valOutputs.add(it)
						]
					}
				}
				
			}
			
			putToContextSIB.addToControlFlow(objCrt)
			successBranch.addToControlFlow(objCrt)
		}
		
		generateListSetter(objCrt, attrDef.key as Attribute, valOutputs)
	}
	
	// TODO move to ProcessGenerationLanguage
	def addFileInputPort(Container dataFlowTarget, String name) {
		PortUtils.addInput(dataFlowTarget, PrimitiveType.FILE) as PrimitiveInputPort => [
			it.name = name
		]
	}
	
	/**
	 * Generates setters for complex non-list attributes
	 */
	def generateComplexSetters(ObjectCreation objCrt, Iterable<AttributeDef> attributeDefs) {
		debug(" generate complex setters for objCrt: " + objCrt)
		attributeDefs
			.filter[valueDef.hasComplexType]
			.forEach[generateComplexSetter(objCrt)]
	}
	
	def generateRead(InputPort inputPort, AttributeValueDef valueDef) {
		debug(" generate primitive setter for input port: " + inputPort?.name)
		val valObjCreatedPort = getValueObjectCreatedPort(valueDef)
		valObjCreatedPort.dataFlowTo(inputPort)
	}
	
	/**
	 * Generates
	 *   - SetAttributeValueSIB for the non-list attribute
	 */
	def generateComplexSetter(AttributeDef attrDef, ObjectCreation objCrt) {
		debug(" generate complex setter for attrDef: " + attrDef?.displayName)
		
		val attr = attrDef.key as Attribute
		val sib = process.newSetAttributeValueSIB(attr, 0,0)
		
		val inputPorts = sib.find(ComplexInputPort)
		
		val objInput = inputPorts.head => [ resize(90,18) ]
		val objCreatedPort = getObjectProvidingPort(objCrt)
		objCreatedPort.dataFlowTo(objInput)
		
		val valInput = inputPorts.last => [ resize(90,18) ]
		
		val valObjCreatedPort = getValueObjectCreatedPort(attrDef.valueDef)
		valObjCreatedPort.dataFlowTo(valInput)
		
		sib.addToControlFlow(objCrt)
		val successBranch = sib.getBranch("success") => [ resize(120,25) ]
		successBranch.addToControlFlow(objCrt)
	}
	
	dispatch def getValueObjectCreatedPort(ObjectCreation valueDef) {
		val attributes = getHierarchicalAttributes(valueDef)
		generateObjectCreation(valueDef, attributes)
		getObjectProvidingPort(valueDef)
	}
	
	dispatch def getValueObjectCreatedPort(AttributeValueReferenceDef valueDef) {
		getObjectProvidingPort(valueDef.value)
	}
	
	def getObjectProvidingPort(NamedObject namedObj) {
		switch it : namedObj {
			ObjectDef: getObjectProvidingPort(it.creation)
			InputDef: getObjectProvidingPort(it)
		}
	}
	
	def getObjectProvidingPort(ObjectCreation objCrt) {
		val port = objCrt_on_port.get(objCrt)
		?: {
			generateObjectCreation(objCrt)
			objCrt_on_port.get(objCrt)
		}
		if (port === null) {
			error("ERROR object providing port is null for " + objCrt)
		}
		return port
	}
	
	def getObjectProvidingPort(InputDef inputDef) {
		val port = inputDef_on_port.get(inputDef)
		if (port === null) {
			error("ERROR object providing port is null for " + inputDef)
		}
		return port
	}
	
	/**
	 * Generates setters for complex list attributes
	 */
	def generateComplexListSetters(ObjectCreation objCrt, Iterable<AttributeDef> attributeDefs) {
		debug(" generate complex list setters for objCrt: " + objCrt)
		attributeDefs
			.forEach[ attrDef | switch valueDef : attrDef.valueDef {
				ComplexListAttributeValuesDef:
					generateComplexListSetter(objCrt, attrDef, valueDef)
			}]
	}
	
	def generateComplexListRead(ComplexInputPort inputPort, ComplexListAttributeValuesDef valueDef, ObjectCreation objCrt) {
		debug(" generate complex list read for InputPort: " + inputPort?.name)
		val valOutputs = generatePutComplexToContextSIB(inputPort.dataType, valueDef, objCrt)
		val variable = generateInputVariable(inputPort)
		for (valOutput : valOutputs) {
			valOutput.dataFlowTo(variable) => [ EdgeLayout.C_LEFT.apply(it) ]
		}
		variable.dataFlowTo(inputPort)
	}
	
	/**
	 * Collects the values as well as the corresponding OutputPorts and generates
	 *   - PutComplexToContextSIB with InputPorts for all OutputPorts of the CreateSIBs
	 *   - SetAttributeValueSIB for the list attribute
	 *   - DataContext with list variable to be filled by the OutputPorts of the PutComplexToContextSIB
	 */
	def generateComplexListSetter(ObjectCreation objCrt, AttributeDef attrDef, ComplexListAttributeValuesDef valueDef) {
		debug(" generate complex list setter for attrDef: " + attrDef?.displayName)
		val attribute = attrDef.key as ComplexAttribute
		val valOutputs = generatePutComplexToContextSIB(attribute.dataType, valueDef, objCrt)
		generateListSetter(objCrt, attribute, valOutputs)
	}
	
	def List<OutputPort> generatePutComplexToContextSIB(Type dataType, ComplexListAttributeValuesDef valueDef, ObjectCreation objCrt) {
		val valOutputs = newArrayList
		val values = valueDef.valuesDef.map[
			switch it {
				ObjectCreation: it
				AttributeValueReferenceDef: value
			}
		].filterNull.toList
		
		if (!values.nullOrEmpty) {
			val putToContextSIB = process.newPutComplexToContextSIB(dataType, 0,0) => [ 
				resize(120,65)
				inputPorts.forEach[delete]
			]
			
			var successBranch = putToContextSIB.getBranch("success") => [
				resize(120,25)
				outputPorts.forEach[delete]
			]
			
			var index = 0
			for (listValDef : valueDef.valuesDef) {
				val valObjCreatedPort = getValueObjectCreatedPort(listValDef)
				val name = "val" + index++
				putToContextSIB.addInputPort(dataType) => [
					resize(90,18)
					it.name = name
					dataFlowFrom(valObjCreatedPort)
				]
				successBranch.addOutputPort(dataType) => [
					resize(90,18)
					it.name = name
					valOutputs.add(it)
				]
			}
			
			putToContextSIB.addToControlFlow(objCrt)
			successBranch.addToControlFlow(objCrt)
		}
		return valOutputs
	}
	
	/**
	 * Generates
	 *   - SetAttributeValueSIB for the list attribute
	 *   - DataContext with list variable to be filled by the specified Outputs
	 */
	def generateListSetter(ObjectCreation objCrt, Attribute attribute, Iterable<? extends Output> valOutputs) {
		val setterSib = process.newSetAttributeValueSIB(attribute, 0,0)
		
		val inputPorts = setterSib.find(InputPort)
		
		val objInput = inputPorts.head => [ resize(90,18) ]
		val objCreatedPort = getObjectProvidingPort(objCrt)
		objCreatedPort.dataFlowTo(objInput)
		
		val valInput = inputPorts.last => [ resize(90,18) ]
		val variable = generateInputVariable(valInput)
		for (valOutput : valOutputs) {
			valOutput.dataFlowTo(variable) => [ EdgeLayout.C_LEFT.apply(it) ]
		}
		
		setterSib.addToControlFlow(objCrt)
		val successBranch = setterSib.getBranch("success") => [ resize(120,25) ]
		successBranch.addToControlFlow(objCrt)
	}
	
	def generateInputVariable(InputPort inputPort) {
		generateInputVariable(inputPort.container as SIB, inputPort)
	}
	
	/**
	 * Generates
	 *  - DataContext with one complex variable with the same type as the InputPort
	 *  - DataFlow from the variable to the InputPort
	 */
	dispatch def generateInputVariable(SIB sib, ComplexInputPort inputPort) {
		val dataContext = process.newDataContext(sib.x - 30, sib.y, 30, sib.height)
		dataContext.newComplexVariable(inputPort.dataType, 
			10, inputPort.middle - 5, // x,y
			10, 10 // width, height
		) => [
			name = inputPort.name + inputPort.id
			isList = inputPort.isList
			dataFlowTo(inputPort)
		]
	}
	
	/**
	 * Generates
	 *  - DataContext with one primitive variable with the same type as the InputPort
	 *  - DataFlow from the variable to the InputPort
	 */
	dispatch def generateInputVariable(SIB sib, PrimitiveInputPort inputPort) {
		val dataContext = process.newDataContext(sib.x - 30, sib.y, 30, sib.height)
		dataContext.newPrimitiveVariable(
			10, inputPort.middle - 5, // x,y
			10, 10 // width, height
		) => [
			dataType = inputPort.dataType
			name = inputPort.name + inputPort.id
			isList = inputPort.isList
			dataFlowTo(inputPort)
		]
	}
	
	def isReferenced(ObjectDef objDef) {
		!objDef.getName.nullOrEmpty
	}
	
	def Collection<AttributeDef> getHierarchicalAttributes(ObjectCreation objCrt) {
		val map = newHashMap
		objCrt.extensionHierarchy.reverse.forEach[
			it.attributes.forEach[map.put(it.name, it)]
		]
		return map.values
	}
	
	/**
	 * Makes the SIB successor of the previous branch and aligns it
	 * in the appropriate column beneath of 'above'. 
	 */
	def addToControlFlow(DataFlowTarget sib, ObjectCreation objCrt) {
		sib => [
			resizeAndLayout
			for (previousBranch : previousBranches) {
				previousBranch.successor = it
			}
			val lastInCol = lastInCol.get(objCrt)
			if (lastInCol !== null) {
				val x = lastInCol.center - sib.width/2
				val lowestBranch = previousBranches.sortBy[bottom].last
				val y = Math.max(lastInCol.bottom, lowestBranch.bottom) + 30
				debug("  align " + sib.eClass.name + " beneath last in col: " + lastInCol + " to " + x + " / " + y + "   lastInCol.bottom: " + lastInCol.bottom + " previousBranch.bottom: " + lowestBranch.bottom)
				it.move(x, y)
				debug("  moved to " + it.x + " / " + it.y)
			} else {
				debug("  align right of rightmostBranch")
				it.move(rightmostBranch.right + 30, previousBranches.sortBy[right].last.middle - sib.height/2)
			}
			find(InputPort)
				.filter[!getIncoming(Read).isEmpty]
				.map[getIncoming(Read).head.sourceElement.container]
				.filter(DataContext)
				.forEach[ dataContext |
					dataContext.move(it.x - 30, it.y)
					dataContext.resize(dataContext.width, it.height)
				]
		]
	}
	
	/**
	 * Aligns the Branch beneath of its SIB and updates the fields
	 * 'previous' as well as 'above'.
	 */
	def addToControlFlow(DataFlowSource branch, ObjectCreation objCrt) {
		branch => [
			resizeAndLayout
			alignBeneathOf(SIBPredecessors.head)
			previousBranch = it
			lastInCol.put(objCrt, it)
		]
	}
	
	def setPreviousBranch(DataFlowSource branch) {
		previousBranches => [
			clear
			add(branch)
		]
	}
	
	def Iterable<ObjectDef> findObjectDefs(EObject model) {
		model.eContents.filter(ObjectDef)
		+ model.eContents.flatMap[findObjectDefs]
	}
	
	// TODO move to de.jabc.cinco.meta.runtime.xapi.CollectionExtension
	def containsAnyOf(Collection<?> c1, Collection<?> c2) {
		!Collections.disjoint(c1,c2)
	}
	
	def getRightmostBranch() {
		process.find(DataFlowSource).sortBy[right].last
	}
	
	def hasBeenCreated(ObjectDef objDef) {
		objCrt_on_port.containsKey(objDef.creation)
	}
	
	def debug(String msg) {
		println("[" + class.simpleName + "|DEBUG] " + msg)
	}
	
	def info(String msg) {
		println("[" + class.simpleName + "] " + msg)
	}
	
	def error(String msg) {
		System.err.println("[" + class.simpleName + "] " + msg)
	}
}
