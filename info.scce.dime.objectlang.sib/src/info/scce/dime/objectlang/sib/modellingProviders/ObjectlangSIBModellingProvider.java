package info.scce.dime.objectlang.sib.modellingProviders;

import java.util.ArrayList;
import java.util.List;

import info.scce.dime.modeltrafo.extensionpoint.buildmodel.GenericOutputBranch;
import info.scce.dime.modeltrafo.extensionpoint.buildmodel.GenericPort;
import info.scce.dime.modeltrafo.extensionpoint.buildmodel.IGenericSIBBuilder;
import info.scce.dime.objectlang.objectLang.ObjectsModel;

public class ObjectlangSIBModellingProvider implements IGenericSIBBuilder<ObjectsModel> {

	@Override
	public String getName(ObjectsModel model) {
		return getLastSegment(model.getName());
	}

	@Override
	public String getLabel(ObjectsModel model) {
		return getLastSegment(model.getName());
	}
	
	protected String getLastSegment(String fqn) {
		String[] segments = fqn.split("\\.");
		if (segments.length>0) {			
			return segments[segments.length-1];
		}
		return fqn;
	}
	
	@Override
	public List<GenericPort> getInputPorts(ObjectsModel model) {
		return new ArrayList<GenericPort>();
	}

	@Override
	public List<GenericOutputBranch> getOutputBranches(ObjectsModel model) {
		List<GenericOutputBranch> outputBranches = new ArrayList<GenericOutputBranch>();
		outputBranches.add(new GenericOutputBranch("success", new ArrayList<GenericPort>()));
		return outputBranches;
	}

}
